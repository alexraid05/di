package com.example.tasca1_1;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.IOException;

public class Tasca1_1 extends Application implements EventHandler<javafx.scene.input.MouseEvent> {

    private Text text = new Text();

    @Override
    public void start(Stage escacs) throws IOException{
        try {
            escacs.setTitle("Escacs");

            Rectangle rectangle1= rectanguloNegro(0,0);
            Rectangle rectangle2= rectanguloBlanco(0,50);
            Rectangle rectangle3= rectanguloNegro(0,100);
            Rectangle rectangle4= rectanguloBlanco(0,150);
            Rectangle rectangle5= rectanguloNegro(0,200);
            Rectangle rectangle6= rectanguloBlanco(0,250);
            Rectangle rectangle7= rectanguloNegro(0,300);
            Rectangle rectangle8= rectanguloBlanco(0,350);

            Rectangle rectangle33= rectanguloBlanco(50,0);
            Rectangle rectangle34= rectanguloNegro(50,50);
            Rectangle rectangle35= rectanguloBlanco(50,100);
            Rectangle rectangle36= rectanguloNegro(50,150);
            Rectangle rectangle37= rectanguloBlanco(50,200);
            Rectangle rectangle38= rectanguloNegro(50,250);
            Rectangle rectangle39= rectanguloBlanco(50,300);
            Rectangle rectangle40= rectanguloNegro(50,350);

            Rectangle rectangle9= rectanguloNegro(100,0);
            Rectangle rectangle10= rectanguloBlanco(100,50);
            Rectangle rectangle11= rectanguloNegro(100,100);
            Rectangle rectangle12= rectanguloBlanco(100,150);
            Rectangle rectangle13= rectanguloNegro(100,200);
            Rectangle rectangle14= rectanguloBlanco(100,250);
            Rectangle rectangle15= rectanguloNegro(100,300);
            Rectangle rectangle16= rectanguloBlanco(100,350);

            Rectangle rectangle41= rectanguloBlanco(150,0);
            Rectangle rectangle42= rectanguloNegro(150,50);
            Rectangle rectangle43= rectanguloBlanco(150,100);
            Rectangle rectangle44= rectanguloNegro(150,150);
            Rectangle rectangle45= rectanguloBlanco(150,200);
            Rectangle rectangle46= rectanguloNegro(150,250);
            Rectangle rectangle47= rectanguloBlanco(150,300);
            Rectangle rectangle48= rectanguloNegro(150,350);

            Rectangle rectangle17= rectanguloNegro(200,0);
            Rectangle rectangle18= rectanguloBlanco(200,50);
            Rectangle rectangle19= rectanguloNegro(200,100);
            Rectangle rectangle20= rectanguloBlanco(200,150);
            Rectangle rectangle21= rectanguloNegro(200,200);
            Rectangle rectangle22= rectanguloBlanco(200,250);
            Rectangle rectangle23= rectanguloNegro(200,300);
            Rectangle rectangle24= rectanguloBlanco(200,350);

            Rectangle rectangle49= rectanguloBlanco(250,0);
            Rectangle rectangle50= rectanguloNegro(250,50);
            Rectangle rectangle51= rectanguloBlanco(250,100);
            Rectangle rectangle52= rectanguloNegro(250,150);
            Rectangle rectangle53= rectanguloBlanco(250,200);
            Rectangle rectangle54= rectanguloNegro(250,250);
            Rectangle rectangle55= rectanguloBlanco(250,300);
            Rectangle rectangle56= rectanguloNegro(250,350);

            Rectangle rectangle25= rectanguloNegro(300,0);
            Rectangle rectangle26= rectanguloBlanco(300,50);
            Rectangle rectangle27= rectanguloNegro(300,100);
            Rectangle rectangle28= rectanguloBlanco(300,150);
            Rectangle rectangle29= rectanguloNegro(300,200);
            Rectangle rectangle30= rectanguloBlanco(300,250);
            Rectangle rectangle31= rectanguloNegro(300,300);
            Rectangle rectangle32= rectanguloBlanco(300,350);

            Rectangle rectangle57= rectanguloBlanco(350,0);
            Rectangle rectangle58= rectanguloNegro(350,50);
            Rectangle rectangle59= rectanguloBlanco(350,100);
            Rectangle rectangle60= rectanguloNegro(350,150);
            Rectangle rectangle61= rectanguloBlanco(350,200);
            Rectangle rectangle62= rectanguloNegro(350,250);
            Rectangle rectangle63= rectanguloBlanco(350,300);
            Rectangle rectangle64= rectanguloNegro(350,350);

            text.setX(0);
            text.setY(420);

            Group root = new Group(rectangle1,rectangle2,rectangle3,rectangle4,rectangle5,rectangle6,rectangle7,rectangle8,rectangle9,rectangle10,rectangle11,rectangle12,rectangle13,rectangle14,rectangle15,rectangle16,rectangle17,rectangle18,rectangle19,rectangle20,rectangle21,rectangle22,rectangle23,rectangle24,rectangle25,rectangle26,rectangle27,rectangle28,rectangle29,rectangle30,rectangle31,rectangle32,rectangle33,rectangle34,rectangle35,rectangle36,rectangle37,rectangle38,rectangle39,rectangle40,rectangle41,rectangle42,rectangle43,rectangle44,rectangle45,rectangle46,rectangle47,rectangle48,rectangle49,rectangle50,rectangle51,rectangle52,rectangle53,rectangle54,rectangle55,rectangle56,rectangle57,rectangle58,rectangle59,rectangle60,rectangle61,rectangle62,rectangle63,rectangle64,text);
            Scene theScene = new Scene(root,420,420);
            escacs.setScene(theScene);
            escacs.show();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    private Rectangle rectanguloNegro(int X, int Y) {
        Rectangle rectangle = new Rectangle(50,50);
        rectangle.setX(X);
        rectangle.setY(Y);
        rectangle.setFill(Color.BLACK);
        rectangle.setOnMouseClicked(this);
        return rectangle;
    }

    private Rectangle rectanguloBlanco(int X, int Y) {
        Rectangle rectangle = new Rectangle(50,50);
        rectangle.setX(X);
        rectangle.setY(Y);
        rectangle.setFill(Color.WHITE);
        rectangle.setOnMouseClicked(this);
        return rectangle;
    }

    public static void main(String[] args) {launch();}

    @Override
    public void handle(javafx.scene.input.MouseEvent mouseEvent) {
        Rectangle rectangle= (Rectangle) mouseEvent.getSource();
        text.setText("Has pulsado la columna "+(int)((rectangle.getX()/50)+1)+" y la fila "+(int)((rectangle.getY()/50)+1)+".");
    }
}