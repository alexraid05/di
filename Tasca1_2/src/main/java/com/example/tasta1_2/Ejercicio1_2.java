package com.example.tasta1_2;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import java.io.IOException;

public class Ejercicio1_2 extends Application {
    @Override
    public void start(Stage circulos) throws IOException{
        try {
            circulos.setTitle("Practica1.1: Escacs");

            //En cada nuevo circulo, llamamos al método circulo y añadimos las posiciones X e Y de cada uno.
            Circle circulo1= circuloRojo(25,375);
            Circle circulo2= circuloRojo(75,375);
            Circle circulo3= circuloRojo(125,375);
            Circle circulo4= circuloRojo(175,375);
            Circle circulo5= circuloRojo(225,375);
            Circle circulo6= circuloRojo(275,375);
            Circle circulo7= circuloRojo(325,375);
            Circle circulo8= circuloRojo(375,375);

            Circle circulo9= circuloRojo(50,325);
            Circle circulo10= circuloRojo(100,325);
            Circle circulo11= circuloRojo(150,325);
            Circle circulo12= circuloRojo(200,325);
            Circle circulo13= circuloRojo(250,325);
            Circle circulo14= circuloRojo(300,325);
            Circle circulo15= circuloRojo(350,325);


            Circle circulo16= circuloRojo(75,275);
            Circle circulo17= circuloRojo(125,275);
            Circle circulo18= circuloRojo(175,275);
            Circle circulo19= circuloRojo(225,275);
            Circle circulo20= circuloRojo(275,275);
            Circle circulo21= circuloRojo(325,275);


            Circle circulo22= circuloRojo(100,225);
            Circle circulo23= circuloRojo(150,225);
            Circle circulo24= circuloRojo(200,225);
            Circle circulo25= circuloRojo(250,225);
            Circle circulo26= circuloRojo(300,225);


            Circle circulo27= circuloRojo(125,175);
            Circle circulo28= circuloRojo(175,175);
            Circle circulo29= circuloRojo(225,175);
            Circle circulo30= circuloRojo(275,175);


            Circle circulo31= circuloRojo(150,125);
            Circle circulo32= circuloRojo(200,125);
            Circle circulo33= circuloRojo(250,125);

            Circle circulo34= circuloRojo(175,75);
            Circle circulo35= circuloRojo(225,75);

            Circle circulo36= circuloRojo(200,25);


            Group root = new Group(circulo1,circulo2,circulo3,circulo4,circulo5,circulo6,circulo7,circulo8,circulo9,circulo10,circulo11,circulo12,circulo13,circulo14,circulo15,circulo16,circulo17,circulo18,circulo19,circulo20,circulo21,circulo22,circulo23,circulo24,circulo25,circulo26,circulo27,circulo28,circulo29,circulo30,circulo31,circulo32,circulo33,circulo34,circulo35,circulo36);
            Scene scene = new Scene(root,425,425);
            circulos.setScene(scene);
            circulos.show();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    //Cuando se llama a este método crea un nuevo circulo con las caráctiristicas descritas.
    private Circle circuloRojo(int X, int Y) {
        Circle circulo = new Circle(20);
        circulo.setCenterX(X);
        circulo.setCenterY(Y);
        circulo.setFill(Color.VIOLET);
        //Hemos añadido en el estilo un parametro que nos añade tanto el borde como el color de este en los circulos.
        circulo.setStyle("-fx-stroke-width: 3;-fx-stroke: black;");
        return circulo;
    }

    public static void main(String[] args) {launch();}
}