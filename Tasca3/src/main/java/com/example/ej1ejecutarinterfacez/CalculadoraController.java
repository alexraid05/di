package com.example.ej1ejecutarinterfacez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class CalculadoraController implements Initializable {

    @FXML
    private TextField calculadoraResultado;

    private LinkedList<String> numbers=new LinkedList();

    private LinkedList<String> operacion =new LinkedList<>();

    private BigDecimal decimales;
    Pattern pattern=Pattern.compile("\\d+|\\d.?\\d");

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void pulseN(ActionEvent event){
        String number = getButtonValue(event);
        String textcalculadoraResultado = calculadoraResultado.getText();
        if(".".equals(number.trim())){
            boolean contains = textcalculadoraResultado.contains(".");
            if(contains||isBlank(textcalculadoraResultado)){
                return;
            }else {
                calculadoraResultado.setText(textcalculadoraResultado+".");
                return;
            }
        }
        calculadoraResultado.setText(calculadoraResultado.getText()+number);
    }


    public void pulseOperación(ActionEvent event){
        String text = calculadoraResultado.getText();

        if (!pattern.matcher(text).matches()) {
            return;
        }
        if (isBlank(text)){
            return;
        }
        numbers.add(text);
        String buttonValue = getButtonValue(event);
        operacion.add(buttonValue);
        calculadoraResultado.setText("");
    }



    public void pulseR(ActionEvent event) {
        boolean numbersEmpty = numbers.isEmpty();
        boolean operacionEmpty = operacion.isEmpty();
        if(numbersEmpty||operacionEmpty){
            calculadoraResultado.setText(calculadoraResultado.getText());
            return;
        }

        if(numbers.size()>0){
            numbers.add(calculadoraResultado.getText());
            decimales =new BigDecimal(numbers.getFirst());
            for (int i=1;i<numbers.size();i++){
                switch (operacion.get(i-1)){
                    case "+":
                        decimales = decimales.add(new BigDecimal(numbers.get(i)));
                        break;
                    case "-":
                        decimales = decimales.subtract(new BigDecimal(numbers.get(i)));
                        break;
                    case "/":
                        BigDecimal decimal = new BigDecimal(numbers.get(i));
                        int compareTo = decimal.compareTo(new BigDecimal("0"));
                        if(compareTo==0){
                            calculadoraResultado.setText ("El divisor no puede ser 0");
                            numbers.clear();
                            operacion.clear();
                            return;
                        }
                        this.decimales = decimales.divide(new BigDecimal(numbers.get(i)), 20, BigDecimal.ROUND_UP);
                        break;
                    case "X":
                        this.decimales = this.decimales.multiply(new BigDecimal(numbers.get(i)));
                        break;

                }
            }
            calculadoraResultado.setText(decimales.stripTrailingZeros().toString());
            numbers.clear();
            operacion.clear();
        }
    }


    public void pulseC(ActionEvent event) {
        calculadoraResultado.setText("");
        operacion.clear();
        numbers.clear();
    }


    private String getButtonValue(ActionEvent event){
        Button button = (Button)event.getSource();
        return button.getText();
    }

    private boolean isBlank(String text){
        if(text!=null&&text!=""&&text.length()>0){
            return false;
        }
        return true;
    }

}
