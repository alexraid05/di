package com.example.ej1ejecutarinterfacez;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FormularioController implements Initializable{

    @FXML
    private TextField nombreF,apellidosF,comentarioF;

    @FXML
    private RadioButton radioBtnM, radioBtnF;

    @FXML
    private ChoiceBox<String> choiceBoxCiudad, choiceBoxSO;

    @FXML
    private Tooltip tipCiudad, tipSO;

    @FXML
    private Label sliderLabel;

    @FXML
    private Slider sliderHoras;

    @FXML
    private DatePicker datePickerFecha;

    private int pcHoras;

    private final String[] ciudades = {"Alcoy","Alicante","Castalla","Ibi"};

    private final String[] sistemasOperativos = {"Microsoft Windows","Mac Os X","Linux"};

    private String generoSelected;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        tipCiudad.setText("Seleccione su ciudad");
        tipSO.setText("Seleccione su sistema operativo");

        choiceBoxCiudad.getItems().addAll(ciudades);
        choiceBoxCiudad.setTooltip(tipCiudad);

        choiceBoxSO.getItems().addAll(sistemasOperativos);
        choiceBoxSO.setTooltip(tipSO);

        sliderHoras.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {

                pcHoras = (int) sliderHoras.getValue();
                sliderLabel.setText(Integer.toString(pcHoras));
            }
        });
    }


    public void onConfirmarClick(ActionEvent event) throws IOException {

        if(radioBtnM.isSelected()){
            generoSelected = "Masculino";
        }else if (radioBtnF.isSelected()){
            generoSelected = "Femenino";
        }

        String nombre = nombreF.getText(), apellidos = apellidosF.getText(), comentario = comentarioF.getText();
        String genero = generoSelected;
        String ciudad = choiceBoxCiudad.getValue();
        String so = choiceBoxSO.getValue();
        String horas = String.valueOf(pcHoras);
        String fecha = String.valueOf(datePickerFecha.getValue());

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("resumen.fxml"));
        Parent root = fxmlLoader.load();

        //Creamos una instancia del controler de la siguiente escena
        ResumenController resumenControllerInstance = fxmlLoader.getController();

        resumenControllerInstance.rellenarDatos(nombre,apellidos,comentario,genero,ciudad,so,horas,fecha);

        Scene scene = new Scene(root, 608, 402);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setTitle("VentanaResumen");
        stage.setScene(scene);
        stage.show();

    }

}
