package com.example.ej1ejecutarinterfacez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {

    @FXML
    protected void onLoginClick() throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("ventana_login.fxml"));

        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root, 380, 230);
        Stage stage = new Stage();
        stage.setTitle("VentanaLogin");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void onCalculadoraClick() throws IOException{

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("calculadora.fxml"));

        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root, 330, 460);
        Stage stage = new Stage();
        stage.setTitle("VentanaCalculadora");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void onFormularioClick(ActionEvent event) throws IOException{

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("formulario.fxml"));

        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root, 606, 490);
        Stage stage = new Stage();
        stage.setTitle("VentanaFormulario");
        stage.setScene(scene);
        stage.show();
    }
}