package com.example.ej1ejecutarinterfacez;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ResumenController {

    @FXML
    private Label lNombre;

    @FXML
    private Label lApellidos;

    @FXML
    private Label lComentario;

    @FXML
    private Label lGenero;

    @FXML
    private Label lCiudad;

    @FXML
    private Label lSO;

    @FXML
    private Label lHoras;

    @FXML
    private Label lFecha;

    public void rellenarDatos(String nombre, String apellidos, String comentario, String genero, String ciudad, String so, String horas, String fecha){
        lNombre.setText(nombre);
        lApellidos.setText(apellidos);
        lComentario.setText(comentario);
        lGenero.setText(genero);
        lCiudad.setText(ciudad);
        lSO.setText(so);
        lHoras.setText(horas);
        lFecha.setText(fecha);
    }


}
