package com.example.ej1ejecutarinterfacez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class VentanaLoginController {

    private String user = "alejandro";

    private String passw = "1234";

    @FXML
    private TextField textUsuario;

    @FXML
    private TextField textPassworld;

    @FXML
    private Label errorLabel;

    @FXML
    protected void onExit(ActionEvent event) {

        this.cerrarVentana(event);
    }

    @FXML
    protected void onLogin(ActionEvent event) {
        if (textUsuario.getText().equals(user) && textPassworld.getText().equals(passw) ){
            errorLabel.setText("Bienvenido "+user);
        }else{
            errorLabel.setText("Error, usuario o contraseña incorrectos");
        }
    }

    @FXML
    public void cerrarVentana(ActionEvent event){

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
