package com.example.tasca3_2;

import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class HelloController extends Application {
    @FXML
    private Rectangle cuadrado;

    RotateTransition rotate = new RotateTransition();

    public void onClickRoteI() {

        rotate.setDuration(Duration.millis(1000));
        rotate.setAxis(Rotate.Z_AXIS);
        rotate.setCycleCount(3);
        rotate.setByAngle(-360);

        rotate.setNode(cuadrado);

        rotate.play();
    }

    public void onClickRoteD() {

        rotate.setDuration(Duration.millis(1000));
        rotate.setAxis(Rotate.Z_AXIS);
        rotate.setCycleCount(3);
        rotate.setByAngle(360);

        rotate.setNode(cuadrado);

        rotate.play();
    }

    @Override
    public void start(Stage stage) throws Exception {


    }
}