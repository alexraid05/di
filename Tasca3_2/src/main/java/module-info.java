module com.example.tasca3_2 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.tasca3_2 to javafx.fxml;
    exports com.example.tasca3_2;
}