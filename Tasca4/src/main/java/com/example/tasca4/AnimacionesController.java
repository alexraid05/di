package com.example.tasca4;

import javafx.animation.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class AnimacionesController implements Initializable {

    @FXML
    private ImageView logoImagen;

    @FXML
    private Label labelBienvenido;

    @FXML
    private Label lbCargando;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        FadeTransition ft = new FadeTransition(Duration.millis(3000), lbCargando);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();

        DropShadow dp = new DropShadow();
        dp.setOffsetX(6.0);
        dp.setOffsetY(4.0);

        ScaleTransition st1 = new ScaleTransition(Duration.millis(2000), labelBienvenido);
        st1.setToX(2f);
        st1.setToY(2f);
        st1.setCycleCount(2);

        st1.play();

        labelBienvenido.setEffect(dp);

        FadeTransition ft2 = new FadeTransition(Duration.millis(3000), logoImagen);
        ft2.setFromValue(1.0f);
        ft2.setToValue(0.3f);
        ft2.setCycleCount(2);
        ft2.setAutoReverse(true);

        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), logoImagen);
        tt.setFromX(50);
        tt.setToX(350);
        tt.setCycleCount(2);
        tt.setAutoReverse(true);

        RotateTransition rt = new RotateTransition(Duration.millis(3000), logoImagen);
        rt.setByAngle(180f);
        rt.setCycleCount(2);
        rt.setAutoReverse(true);

        ScaleTransition st = new ScaleTransition(Duration.millis(2000), logoImagen);
        st.setToX(2f);
        st.setToY(2f);
        st.setCycleCount(2);
        st.setAutoReverse(true);

        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().addAll(
                ft,
                tt,
                rt,
                st);
        pt.setCycleCount(Timeline.INDEFINITE);
        pt.play();

    }
}
