module com.example.tasca4 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.tasca4 to javafx.fxml;
    exports com.example.tasca4;
}