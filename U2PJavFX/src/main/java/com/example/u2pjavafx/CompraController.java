package com.example.u2pjavafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.input.ClipboardContent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CompraController implements Initializable {

    @FXML
    ImageView imageView1;

    @FXML
    FlowPane fTarget, fPaneInicial;

    @FXML
    Label lUsuario;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        insertImagen(new Image("file:src/main/resources/com/example/imagenes/BS1.png"), fPaneInicial);
        insertImagen(new Image("file:src/main/resources/com/example/imagenes/BS2.png"), fPaneInicial);
        insertImagen(new Image("file:src/main/resources/com/example/imagenes/BS3.png"), fPaneInicial);
        insertImagen(new Image("file:src/main/resources/com/example/imagenes/BS4.png"), fPaneInicial);
        insertImagen(new Image("file:src/main/resources/com/example/imagenes/BS5.png"), fPaneInicial);

        setupGestureTarget(fTarget);
        setupGestureTarget(fPaneInicial);
    }

    private void insertImagen(Image i, FlowPane fPaneInicial) {
        imageView1 = new ImageView();
        imageView1.setFitWidth(70);
        imageView1.setFitHeight(100);
        imageView1.setImage(i);

        setupGestureSource(imageView1);
        fPaneInicial.getChildren().add(imageView1);
    }

    private void setupGestureSource(ImageView source){
        source.setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Dragboard dragboard = source.startDragAndDrop(TransferMode.MOVE);

                ClipboardContent content = new ClipboardContent();
                Image sourceImage = source.getImage();
                content.putImage(sourceImage);
                dragboard.setContent(content);

                imageView1 = source;

                mouseEvent.consume();
            }
        });

        source.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                source.setCursor(Cursor.HAND);
            }
        });
    }

    private void setupGestureTarget(FlowPane fPane){

        fPane.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                Dragboard dragboard = dragEvent.getDragboard();
                if(dragboard.hasImage()){
                    dragEvent.acceptTransferModes(TransferMode.MOVE);
                }
                dragEvent.consume();
            }
        });

        fPane.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                Dragboard dragboard = dragEvent.getDragboard();

                if(dragboard.hasImage()){
                    imageView1.setImage(dragboard.getImage());
                    fPane.getChildren().add(imageView1);
                    dragEvent.setDropCompleted(true);
                }else {
                    dragEvent.setDropCompleted(false);
                }

                dragEvent.consume();
            }
        });
    }


    public void OnClickConfirmar(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("inicio.fxml"));
        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("| Books Valley App |");
        stage.setScene(scene);
        stage.show();

        cerrarVentana(event);

    }

    @FXML
    public void cerrarVentana(ActionEvent event){

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
