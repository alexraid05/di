package com.example.u2pjavafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class InicioController implements Initializable {

    @FXML
    Button buttonLogin, bCerrarSesion;

    @FXML
    Label lUsuario;

    @FXML
    Text l1,l2,l3,l4,l5;

    @FXML
    TextField targetFld;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(Manager.usuarioActual!=null){
            lUsuario.setText(Manager.usuarioActual.getNombre());
            buttonLogin.setVisible(false);
            bCerrarSesion.setVisible(true);
        }else{
            buttonLogin.setVisible(true);
            bCerrarSesion.setVisible(false);

        }
    }

    public void onClickLogin(ActionEvent event) {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login.fxml"));

        Parent root = null;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("VentanaLogin");
        stage.setScene(scene);
        stage.show();

        cerrarVentana(event);
    }

    @FXML
    public void cerrarVentana(ActionEvent event){

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public void mostrarUsuario(String usuario){
        lUsuario.setText(usuario.toUpperCase(Locale.ROOT));
        buttonLogin.setVisible(false);
        bCerrarSesion.setVisible(true);
    }


    public void OnClickOut(ActionEvent event) {
        lUsuario.setText("");
        Manager.usuarioActual = null;
        buttonLogin.setVisible(true);
        bCerrarSesion.setVisible(false);
    }

    public void OnClickAcceder(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("compra.fxml"));

        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("Compra");
        stage.setScene(scene);
        stage.show();

        cerrarVentana(event);
    }
}
