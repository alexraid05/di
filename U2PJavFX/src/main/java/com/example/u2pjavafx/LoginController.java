package com.example.u2pjavafx;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    TextField cCorreo, nNombre, nCorreo, nTélefono, nDirección;

    @FXML
    PasswordField cContraseña, nContraseña, nConfirmar;

    @FXML
    Label lIncorrecto, l1,l2,l3,l4, lUsuarioContraseña;

    public List<Usuario> usuarios = Manager.usuarios;

    @FXML
    ImageView iLogo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        TranslateTransition transition = new TranslateTransition(Duration.seconds(2), iLogo);
        transition.setFromX(820);
        transition.setToX(-1.0 * iLogo.getLayoutBounds().getWidth());
        transition.setCycleCount(TranslateTransition.INDEFINITE);
        transition.setAutoReverse(true);
        transition.play();

    }

    public void OnClickAcceso(ActionEvent event) throws IOException {

        for (int i = 0;i< usuarios.size();i++){
            if(usuarios.get(i).getCorreo().equals(cCorreo.getText()) && usuarios.get(i).getContraseña().equals(cContraseña.getText())){

                FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("inicio.fxml"));
                Parent root = fxmlLoader.load();

                InicioController inicioControllerInstance = fxmlLoader.getController();
                inicioControllerInstance.mostrarUsuario(usuarios.get(i).getNombre());

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("| Books Valley App |");
                stage.setScene(scene);
                stage.show();

                lUsuarioContraseña.setText("");
                cerrarVentana(event);
            }
        }

        for (int i = 0;i< usuarios.size();i++){
            if(!usuarios.get(i).getCorreo().equals(cCorreo.getText())){
                lUsuarioContraseña.setText("Usuario o Contraseña Incorrectos");
            }
        }
    }

    public void OnClickCrearCuenta(ActionEvent event) throws IOException {
        lIncorrecto.setText("");
        l1.setText("");
        l2.setText("");
        l3.setText("");
        l4.setText("");

        if((nCorreo.getText() != null) && (nContraseña.getText() !=null)  && (nConfirmar.getText() != null) &&(nContraseña.getText().equals(nConfirmar.getText()))){
            Usuario usuario = new Usuario();
            usuario.setNombre(nNombre.getText());
            usuario.setCorreo(nCorreo.getText());
            usuario.setContraseña(nContraseña.getText());
            usuario.setNumTelefono(nTélefono.getText());
            usuario.setDirección(nDirección.getText());

            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("resumen.fxml"));
            Parent root = fxmlLoader.load();

            ResumenController resumenControllerInstance = fxmlLoader.getController();

            resumenControllerInstance.rellenarDatos(usuario.getNombre(),usuario.getCorreo(), usuario.getNumTelefono(), usuario.getDirección());

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Resumen");
            stage.setScene(scene);
            stage.show();

            Manager.usuarios.add(usuario);
            Manager.usuarioActual = usuario;
            nCorreo.setText(null);
            nContraseña.setText(null);
            nConfirmar.setText(null);
            cerrarVentana(event);

        }else{
            l1.setText("*");
            l2.setText("*");
            l3.setText("*");
            l4.setText("*");
            lIncorrecto.setText("Rellena los campos obligatorios correctamente");
        }
    }

    @FXML
    public void cerrarVentana(ActionEvent event){

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
