package com.example.u2pjavafx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class ResumenController {

    @FXML
    private Label lCorreo, lTeléfono, lDirección, lNombre;

    public void rellenarDatos(String nombre,String correo, String telefono, String direccion){
        lNombre.setText(nombre);
        lCorreo.setText(correo);
        lTeléfono.setText(telefono);
        lDirección.setText(direccion);
    }

    public void OnClickContinuar(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("inicio.fxml"));
        Parent root = fxmlLoader.load();

        InicioController inicioControllerInstance = fxmlLoader.getController();
        inicioControllerInstance.mostrarUsuario(lNombre.getText());

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("| Books Valley App |");
        stage.setScene(scene);
        stage.show();

        cerrarVentana(event);

    }

    @FXML
    public void cerrarVentana(ActionEvent event){

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
