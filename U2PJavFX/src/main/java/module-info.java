module com.example.u2pjavafx {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.u2pjavafx to javafx.fxml;
    exports com.example.u2pjavafx;
}